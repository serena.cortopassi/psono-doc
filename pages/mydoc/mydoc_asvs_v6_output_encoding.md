---
title: V6 Output encoding
tags: [asvs, owasp]
summary: "Output encoding / escaping"
sidebar: mydoc_sidebar
permalink: mydoc_asvs_v6_output_encoding.html
folder: mydoc
---


This section was incorporated into V5 in Application Security Verification Standard 2.0.
ASVS requirement 5.16 addresses contextual output encoding to help prevent Cross Site
Scripting. 


